from time import time
from Page import Page

class Website:
  def __init__(self, pages=0):
    self.current = 0
    self.pages = pages
    self.cache = {}

  def __iter__(self):
    return self
  
  def __getitem__(self, page):
    return self.getPage(page);

  def getImages(self, no):
    """
    Should return images from page nr. "page".
    first page = newest
    last page = oldest.
    """
    raise NotImplementedError;
  
  def getPage(self, no):
    try:
      (last_checked, page)=self.cache[no];
      if (time()-last_checked) > 60:  # One-minute cache.
        self.cache[no]=(last_checked,page)=(time(),Page(self.getImages(no),no));
    except KeyError as e:
      self.cache[no]=(last_checked,page)=(time(),Page(self.getImages(no),no));
    return page

  def __len__(self):
    return self.pages;

  def __next__(self):
    if self.current >= self.pages:
      raise StopIteration
    else:
      self.current += 1
      return self[self.current - 1]
