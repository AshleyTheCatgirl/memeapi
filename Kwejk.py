from Website import Website
from Image import Image
from Page import Page

from urllib.request import urlopen

from time import time
from time import strptime
from time import strftime

from re import match

import lxml.html


class Kwejk(Website):
  def __init__(self):
    self.mainpage=lxml.html.fromstring(urlopen("http://kwejk.pl/").read().decode('utf-8'));page_count_xpath='.//span[@class="current page"]';
    super().__init__(int(self.mainpage.find(page_count_xpath).text));
  
  def getImages(self, no):
    self.mainpage=lxml.html.fromstring(urlopen("http://kwejk.pl/").read().decode('utf-8'));page_count_xpath='.//span[@class="current page"]';
    self.pages=int(self.mainpage.find(page_count_xpath).text);
    pageno=self.pages-no;
    page_html=lxml.html.fromstring(urlopen("http://kwejk.pl/page/%s"%(pageno,)).read().decode('utf-8'));
    images_html=page_html.findall(".//img")
    images=[];
    for image in images_html:
      if not isinstance(match('^http://i[0-9]\.kwejk\.pl/site_media/obrazki/.*',image.attrib['src']),None.__class__):
        try:
          url=image.attrib['src'];
          title=image.attrib['alt'];
          page="http://kwejk.pl%s"%(image.getparent().attrib['href'],)
          if not page.endswith("gallerypic"):
            img_page_html=lxml.html.fromstring(urlopen(page).read().decode('utf-8'));
            date=img_page_html.find('.//span[@class="date"]').text[2:];
            date=strptime(date,"%d.%m.%Y %H:%M")
            date=int(strftime('%s',date));
            images.insert(0,Image(title=title,url=url,page=page,upload_date=date));
        except KeyError as e:
          if not e.__str__()=="'href'":
            raise
    return images
