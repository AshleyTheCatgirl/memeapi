class Page():
  def __init__(self,images,no):
    self.images=sorted(images, key=lambda x: x.upload_date, reverse=True);
    self.no=no;
    self.low=0;
  
  def __getitem__(self, no):
    return self.images[no];

  def __iter__(self):
    return self;

  def __next__(self):
    try:
      return self.images[self.low];
    except:
      raise StopIteration;
    finally:
      self.low+=1;

